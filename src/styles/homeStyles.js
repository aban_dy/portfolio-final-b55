import styled from 'styled-components';
import { motion } from 'framer-motion';



export const Banner = styled.div`
	background: ${ ({theme}) => theme.background };
	height: 100vh;
	width: 100%;
	position: relative;
	margin-bottom: 296px;
	@media(max-width: 768px){
		height: 50vh;
		margin-bottom: 100px;
	}
`

export const Video = styled.div`
	height: 100%;
	width: 100%;
	background-atttachment: fixed;
	position: relative;
	display: flex;
	justify-content: flex-end;
	align-items: center;
	video{
		object-fit: cover;
		width: 100%;
		height: 100%;
	}
	
`

export const Canvas = styled.canvas`
	position: relative;
	height: 100%;
	width: 100%;
	display: block;
	
`
export const BannerTitle = styled(motion.div)`
	color: ${({ theme }) => theme.text};
	pointer-event: none;
	position: absolute;
	z-index: 99;
	left: -2%;
	bottom: -20%;
	overflow: hidden;
`

export const HeadLine = styled(motion.span)`
	display: block;
	font-size: 20vw;
	font-weight: 900;
	line-height: .76;
`