import styled from 'styled-components';

export const FixedContainer = styled.div`
	position: sticky;
	top: 50%;
	transform: translateY(-50%);
	margin-left: -2%;
	height: 100%;
	width: 100px;
	backgound: none;
	z-index: 99;
	ul{
		width: 100%;
		list-style: none;
		position: relative;
		display: flex;
		flex-direction: column;
		align-items: flex-start;
		text-center;
		background: ${({theme}) => theme.background};
		li{
			font-weight: 900;
			font-size: 1.2rem;
			display: block;
			width: 100%;
			text-align: center;
			color: ${({theme}) => theme.text};
		}
		span{
			height: 150%;
			width: 2px;
			display: block;
			background: red;
			position: absolute;
			top: -25%;
			left: 63.9%;
			z-index: -1
		}
	}

	@media(max-width: 768px){
		display: none;
	}

`;