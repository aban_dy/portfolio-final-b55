import styled from 'styled-components';
import { motion } from 'framer-motion';


export const AboutContainer = styled.div`
	width: 100%;
	height: 100%;
	margin: 0;
	background: ${({ theme }) => theme.background};
	position: relative;
	margin: 1rem auto;
	overflow: hidden;
	@media(max-width: 768px){
		width: 100%;
		}
	}
`;

export const Inner = styled.div`
	display: flex;
	align-items: center;
	justify-content: center;
	width: 100%;
	height: 100%;
	@media(max-width: 768px){
		flex-direction: column;
	}
`;

export const Content = styled(motion.div)`
	width: 100%;
	height: 100%;
	display: flex;
	justify-content: center;
	align-items: center;
`;
export const Text = styled.div`
	width: 60%;
	height: 60%;
	h1{
		text-transform: uppercase;
		font-weight: 900;
		font-size: 1.8rem;
	}
	h3{
		font-weight: 900;
		font-size: 1.2rem;
		text-transform: uppercase;
	}
	p{
		font-weight: 400;
		font-size: 1rem;
		margin-bottom: 1rem;
	}
`;

export const Images = styled(motion.div)`
	width: 100%;
	height: 100%;
	position: relative;
`;

export const TopRight = styled(motion.div)`
	width: 100%;
	text-align: right;
	z-index: 9;
	position: relative;
	img{
		width: 50%;
		object-fit: cover;
		border-radiud: .2rem;
	}
`;

export const TopLeft = styled(motion.div)`
	width: 100%;
	position: relative;
	z-index: 9;
	img{
		width: 50%;
		object-fit: cover;
		border-radiud: .2rem;
		margin:0;

	}
`;

export const BlackBox = styled.div`	
	width: 100%;
	height: 50%;
	position: absolute;
	top: 50%;
	left: 50.25%;
	transform: translate(-50%,-50%);
	background: ${({theme}) => theme.text};
	z-index: 1;
`;

export const BoxOverlay = styled.div`
	width: 100%;
	height: 50%;
	position: absolute;
	top: 50%;
	left: 50%;
	transform: translate(-50%,-50%);
	background: ${({theme}) => theme.red};
	mix-blend-mode: overlay;
	z-index: 11;
`;



