import styled from 'styled-components';
import { motion } from 'framer-motion';

export const ContactContainer = styled(motion.div)`
	height: auto;
	width: 60%;
	margin: 100px auto;
	position: relative;
	@media(max-width: 768px){
		width: 80%;
	}
`;

export const Video = styled(motion.div)`
	height: 100%;
	width: 100%;
	position: relative;
	video{
		width: 100%;
		height: 100%;
		object-fit: cover;
		outline: none;
	}
	h1{
			width: 100%;
			text-transform: uppercase;
			font-size: 3rem;
			font-weight: 900;
			text-align: center;
			margin-bottom: -1.5rem;
			z-index: 99;
			position: relative;
			@media(max-width: 768px){
				font-size: 1.5rem;
				margin-bottom: -.8rem;
			}
		}
`;

export const Form = styled(motion.div)`
	height: auto;
	width: 100%;
	h1{
		text-transform: uppercase;
		font-size: 1.8rem;
	}
	.form-group{
		width: 100%;
		display: flex;
		justify-content: space-around;
		align-items: center;
		margin-bottom: 1rem;
		.form{
			input{
				width: 90%;
			}
		}
		@media(max-width: 768px){
			flex-direction: column;
			.form{
				input{
					margin-bottom: 1rem;
					width: 100%;
				}
			}
		}
	}
	.form{
		width: 100%;
		display: flex;
		flex-direction: column;
		align-items: flex-start;
		justify-content: center;
		label{
			padding: 1rem 0;
		}
		input{
			outline: none;
			border-style: none;
			background: none;
			padding: 6px 0;
			border-bottom: 1px solid ${({theme}) => theme.text};
			color: ${({theme}) => theme.text};
			transition: all .2s ease-in-out;
			transition-property: width;
			will-change: width;
		}
		input:focus{
			border-bottom: 1px solid ${({theme}) => theme.red};
			width: 95%;
		}
		textarea{
			width: 100%;
			outline: none;
			background: none;
			border-style: none;
			border-bottom: 1px solid ${({theme}) => theme.text};
			color: ${({theme}) => theme.text};
		}
		textarea:focus{
			border-bottom: 1px solid ${({theme}) => theme.red};
		}
		strong{
			color: ${({theme}) => theme.red};
			margin-top: 1rem;
		}
	}
	.btn{
		border-style: none;
		background: none;
		outline: none;
		margin-top: 1rem;
		padding: 1rem 2rem;
		background: none;
		border: 1px solid ${({theme}) => theme.red};
		border-radius: .2rem;
		color: ${({theme}) => theme.text};
		text-transform: uppercase;
		transition: all .2s ease-in-out;
		&:hover{
			background: ${({theme}) => theme.red};
			color: white;
		}
		@media(max-width: 498px){
			display: block;
			width: 100%;
		}
	}

`;

