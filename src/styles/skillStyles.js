import styled from 'styled-components';
import { motion } from 'framer-motion';

export const SkillsContainer = styled(motion.div)`

	height: auto;
	width: 60%;
	margin: 0 auto;
	padding: 0;
	position: relative;
	margin-top: 100px;
	@media(max-width: 768px){
		margin-top: 100px;
	}
`;

export const SkillsTitle = styled.span`
	text-transform: uppercase;
	h1{
		font-size: 2rem;
		font-weight: 900;
		text-align: center;
	}

`;

export const Grid = styled.div`
	height: 100%;
	width: 100%;
	margin: 0 auto;
	display: grid;
	grid-gap: 1rem;
	grid-template-columns: repeat(auto-fit,minmax(200px,1fr));
	@media(max-width: 498px){
		grid-template-columns: repeat(auto-fit,minmax(200px,1fr));
	}
`;

export const Tile = styled(motion.div)`
	padding: 1rem;
	position: relative;
	svg{
		width: 100%;
		height: 100%;
		object-fit: fill;
		path{
			fill: ${({theme}) => theme.text };
		}
	}
	span{
		position:absolute;
		color: ${({theme}) => theme.red};
		top: 50%;
		left: 50%;
		transform: translate(-50%,-50%);
		display: block;
		font-size: 1.2rem;
		font-weight: 200;
		font-family: 'Holtwood One SC', serif;
	}
	@media(max-width: 768px){
		width: 200px;
		margin: 0 auto;
	}
	@media(max-width: 498px){
		width: 200px;
		margin: 0 auto;
	}
`;