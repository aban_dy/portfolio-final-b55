import styled from 'styled-components';
import { motion } from 'framer-motion';

export const Nav = styled(motion.div)`
	position: fixed;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	display: block;
	background: ${({ theme }) => theme.background};
	color: ${({theme}) => theme.text};
	z-index: 100;
	overflow: hidden;
`;

export const NavHeader = styled.div`
	top: 72px;
	position: relative;
	h2{
		color: ${({theme}) => theme.text};
	}
`;

export const CloseNav = styled.div`
	button{
		transform-origin: center;
		border: none;
		padding: 20px;
		background: none;
		outline: none;
		span{
			width: 36px;
			height: 8px;
			display: block;
			background: ${({ theme }) => theme.text};
			margin: 8px;
		}
	}
	
`;

export const NavList = styled.div`
	height: 100%;
	width: 100%;
	display: flex;
	align-items: center;
	ul{
		padding: 0;
		li{
			list-style: none;
			font-size: 1.8rem;
			text-transform: uppercase;
			font-weight: 900;
			height: 64px;
			line-height: 64px;
			overflow: hidden;
			a{
				text-decoration: none;
				color: ${({ theme }) => theme.text};
				mix-blend-mode: difference;
			}
			.link{
				background: none;
				position: relative;
				display: flex;
				align-items: center;
				.arrow{
					height: 64px;
					margin-right: 8px;
					svg{
						width: 48px;
						path{
							fill : ${({ theme }) => theme.text};
						}
					}
				}
			}
		}
	}
`;

export const NavFooter = styled.div`
	
`;

export const NavVideos = styled.div`
	position: absolute;
	top: 0;
	bottom: 0;
	left: 28%;
	z-index: -1;
	height: 100%;
	width: 100%;
	overflow: hidden;
	background: ${({ theme }) => theme.background}; 
	.reveal{
		width: 100%;
		background: ${({ theme }) => theme.background};
		position: absolute;
		top: 0;
		bottom: 0;
		left: 0;
	}
	.video{
		background: ${({ theme }) => theme.background};
		position: absolute;
		height: 100%;
		margin: 0;
		z-index: -1;
		video{
			height: 100%;
			object-fit: cover;
		}
	}
`;