import styled from 'styled-components';
import { motion } from 'framer-motion';

export const ProjectContainer = styled(motion.div)`
	position: relavtive;
	width: 60%;
	margin: 0 auto;
	height: auto;
	margin-top: 100px;
	background: ${({theme}) => theme.background};
	h1{
		font-size: 2rem;
		font-weight: 900;
		text-align: center;
		text-transform: uppercase;
	}
	@media(max-width: 428px){
		width: 80%;
	}
`;
export const Grid = styled(motion.div)`
	width: 100%;
	display: grid;
	grid-template-columns: repeat(auto-fill,minmax(300px,1fr));
	grid-gap: 1rem;
	margin: 0 auto;
`;

export const ProjectItem = styled(motion.div)`
	width: 100%;
	height: 100%;
	margin-bottom: 100px;
`;

export const Video = styled.div`
	width: 100%;
	height: 250px;
	video{
		width: 100%;
		height: 100%;
		object-fit: cover;
		border-radius: .2rem;
		outline: none;
		cursor: none;
	}
`;

export const ProjectTitle = styled.div`
	color: ${({theme}) => theme.text};
	min-height: 100px;
	h1{
		text-align: left;
		font-size: 1.2rem;
		color: ${({theme}) => theme.red};
	}
`;

export const Actions = styled.div`
	width: 100%;
	button{
		border-style: none;
		outline: none;
		padding: .5rem 1rem;
		border-radius: .2rem;
		background: none;
		border: 1px solid ${({theme}) => theme.red};
		margin-right: 1rem;
		text-transform: uppercase;
		transition: all .2s ease-in-out;
		a{
			color: ${({theme}) => theme.text};
		}
		&:hover{
			background: ${({theme}) => theme.red};
			color: white;
		}
	}
	.view-page{
		background: ${({theme}) => theme.red};
		&:hover{
			color: white;
		}
	}
`; 