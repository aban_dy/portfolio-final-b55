import styled from 'styled-components';
import { motion } from 'framer-motion';

export const FooterContainer = styled(motion.div)`
	height: 40vh ;
	width: 60%;
	margin: 0 auto;
	padding: 0;
	display: flex;
	align-items: center;
	justify-content: center;
	@media(max-width: 498px){
		height: auto;
		flex-direction: column;
	}
`;

export const ContactDetails = styled.div`
	height: 100%;
	width: 50%;
	display: flex;
	flex-direction: column;
	align-items: center;
	justify-content: center;
	.contact{
		height: 100%;
		display: flex;
		flex-direction: column;
		align-items: center;
		justify-content: flex-start;
		@media(max-width: 498px){	
			width: 100%;
			align-items: flex-start;
		}

		h1{
			margin: 0;
			text-transform: uppercase;
			font-size: 1.3rem;
			height: 64px
			font-weight: 900;
			color: ${({theme}) => theme.red};
		}
		p{
			font-size: .8rem;
			font-weight: 300;
			word-breal: break;
			margin-bottom: 1rem;
		}
	}
	
	@media(max-width: 498px){
		width: 100%;
		flex-direction: column;
		align-items: center;

	}
`;

export const SocialLinks = styled.div`
	width: 50%;
	height: 100%;
	display: flex;
	flex-direction: column;
	align-items: flex-start;
	justify-content: flex-start;
	overflow: hidden;
	transition: all .4s ease-in-out;
	@media(max-width: 498px){
		width: 100%;
	}
	.social{
		width: 100%;
		background: none;
		position: relative;
		display: flex;
		align-items: center;
		a{
			margin: 0;
			font-size: 1.3rem;
			font-weight: 900;
			text-transform: uppercase;
			height: 64px;
			text-decoration: none;
			color: ${({ theme }) => theme.text};
			&:hover{
				color: ${({ theme }) => theme.red};
			}
			@media(max-width: 498px){
				font-size: 1rem;
			}
		}
		.arrow{
			height: 64px;
			margin-right: 8px;
			svg{
				width: 48px;
				path{
					fill : ${({ theme }) => theme.text};
			}
			@media(max-width: 498px){
				height: 1rem;
			}
		}
	}
	
`;