import styled,{ css } from 'styled-components';

export const Container = styled.div`
	flex-grow: 1;
	margin: 0 auto;
	padding: 32px;
	position: relative;
	width: auto;
	height: 100%;


	@media(min-width: 768px){
		max-width: calc(100vw - 5rem);
	}

	@media(min-width: 960px){
		max-width: 768px;
	}

	@media(min-width: 1280px){
		max-width: 1132px;
	}

	@media(min-width: 1440px){
		max-width: 1280px;
	}

	${ ({fluid}) => fluid
			&& css`
			padding: 0;
			margin: 0;
			max-width 100%;
		`}
`;

export const Flex = styled.div`
	position: relative;
	display: flex;
	align-items: center;

	${({spaceBetween}) => spaceBetween && css`
		justify-content: space-between;
	`}

	${({alignStart}) => alignStart && css`
		align-items: flex-start;
	`}

	${({flexEnd}) => flexEnd && css`
		justify-content: flex-end;
	`}

	${({alignTop}) => alignTop && css`
		align-items: top;
	`}

	${({noHeight}) => noHeight && css`
		height: 0;
	`}

	${({flexRow}) => flexRow && css`
		flex-direction: row;
	`}

	${({flexColumn}) => flexColumn && css`
		flex-direction: column;
	`}
`;	

export const Cursor = styled.div`
	position: absolute;
	top: 0;
	left: 0;
	width: 32px;
	height: 32px;
	background: ${ ({theme}) => theme.red };
	border-radius: 100%;
	transform: translate(-50%,-50%);
	transition: all .1s ease-in-out;
	transition-property: width, height, border;
	will-change: width, height, border;
	pointer-events: none;
	z-index: 999;
	&.pointer {
		border: 2px solid ${ ({theme}) => theme.text } !important;
	}
	&.hovered{
		background: transparent !important;
		border: 2px solid ${ ({theme}) => theme.red };
	}

	&.nav-open{
		background: transparent !important;
		border: 2px solid ${ ({theme}) => theme.red };
	}
	@media(max-width: 768px){
		display: none;
	}

`;

