import React from 'react';

//Components
import Layout from './components/Layout';
import HomeBanner from './components/homePage/HomeBanner';
import About from './components/homePage/About';
import Skills from './components/homePage/Skills';
import Contact from './components/homePage/Contact';
import Project from './components/homePage/Project';
import Footer from './components/homePage/Footer';
import Fixed from './components/Fixed';

//Provider
import { GlobalProvider } from './context/globalContext';


const App = () => {

return(
	<GlobalProvider>
		<Layout>
			<HomeBanner />
			<Fixed />
			<About />
			<Project />
			<Skills />
			<Contact />
			<Footer />
		</Layout>
	</GlobalProvider>
	)
}

export default App;
