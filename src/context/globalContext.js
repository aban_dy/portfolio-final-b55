import React , { createContext, useReducer, useContext } from 'react';

//define the context;
const GlobalStateContext = createContext();
const GlobalDispatchContext = createContext();

//Reducer
const GlobalReducer = (state, action) => {
	switch(action.type){
		case 'TOGGLE_THEME' : {
			return {
				...state,
				currentTheme : action.theme
			}
		}
		case 'CURSOR_TYPE' : {
			return {
				...state,
				cursorType : action.cursorType
			}
		}
		case 'TOGGLE_MENU' : {
			return{
				...state,
				toggleMenu : action.toggleMenu
			}
		}
		case 'CURRENT_PAGE' : {
			return{
				...state,
				currentPage : action.page
			}
		}
		default: {
			return null
		}
	}
}

//Provider
export const GlobalProvider = ({ children }) => {
	
	const [state,dispatch] = useReducer(GlobalReducer,{
		currentTheme: window.localStorage.getItem('theme') === null ? 'dark' : window.localStorage.getItem('theme'),
		cursorType: false,
		cursorStyles: ["pointer","hovered"],
		toggleMenu: false,
		currentPage: "about"
	});

	return(
		<GlobalDispatchContext.Provider value={ dispatch }>
			<GlobalStateContext.Provider value={ state }>
				{ children }
			</GlobalStateContext.Provider>
		</GlobalDispatchContext.Provider>
	)
}

export const useGlobalStateContext = () => useContext(GlobalStateContext);
export const useGlobalDispatchContext = () => useContext(GlobalDispatchContext);