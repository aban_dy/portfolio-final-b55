import React from 'react';
import styled from 'styled-components';
import { motion } from 'framer-motion';

const ToastContainer = styled(motion.div)`
	position: sticky;
	bottom: 12px;
	left: 100%;
	width: 400px;
	height: 200px;
	background: ${({theme})=> theme.text};
	border-radius: .2rem;
	box-shdow: 4px 4px 8px rbgba(0,0,0,.25);
	z-index: 99;
	padding: 1rem;
	display: flex;
	flex-direction: column;
	align-items: center;
	justify-content: space-between;
	h1{
		color: ${({theme})=> theme.background};
		font-size: 1.8rem;
		font-wreight: 900;
		text-transform: capitalize;
		text-align: center;
	}
	button{
		text-align: center;
		width: 100%;
		display: block;
		text-transform: uppercase;
		padding: .5rem 1rem;
		background: ${({theme})=> theme.background};
		color: ${({theme})=> theme.text};
		border-style: none;
		transition: all .2s ease-in-out;
		&:hover{
			color: ${({theme})=> theme.red};
		}

	}
	@media(max-width: 498px){
		width: 200px;
		height: 100px;
		h1{
			font-size: 1.2rem;
		}
	}
`;


const Toast = ({ toastMessage, isOpen, setIsOpen }) => {

const close = () =>{
	setIsOpen(false);
}



React.useEffect(() => {
	const autoClose = () => {
		setTimeout(() => {
			setIsOpen(false);
		},3000)
	}

	if(isOpen){
		autoClose();
	}
},[isOpen,setIsOpen]);




const settings = {
	initial: { opacity: 0 , x: 100 },
	animate:{
		opacity: 1,
		x: 0
	}
}

return(
		<React.Fragment>
			<ToastContainer
				variants={ settings }
				initial="initial"
				animate={ isOpen ? "animate" : "initial" }
			>
				<h1>{ `${toastMessage === "" ? "Message" : toastMessage}` }</h1>
				<button
					onClick={ close }
				>close</button>
			</ToastContainer>
		</React.Fragment>
	)
}
export default Toast;