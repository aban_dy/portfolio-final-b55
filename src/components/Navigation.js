import React,{ useState } from 'react';
import { motion, AnimatePresence } from 'framer-motion';
//styled components

import { Container, Flex } from '../styles/globalStyles';
import { 
	Nav, 
	NavHeader, 
	CloseNav, 
	NavList, 
	NavFooter,
	NavVideos
	} from '../styles/navigationStyles';

const navRoutes = [
	{ id: 0 , title: 'Flames', path: 'https://dy-flames-app.herokuapp.com/', video:'flames.mp4' },
	{ id: 1 , title: 'Washly', path: 'https://aban_dy.gitlab.io/capstone-1', video:'washly.mp4' },
	{ id: 2 , title: 'Polster', path: 'https://glacial-atoll-69513.herokuapp.com/', video:'polster.mp4' },
	{ id: 3 , title: 'Orange', path: 'https://capstone-3-frontent.netlify.app/', video:'orange.mp4' },
]

const Navigation = ({toggleMenu,setToggleMenu}) => {

	const [revealVideo, setRevealVideo] = useState({
		show: false,
		video: 'flames.mp4',
		key: "0"
	})

	return(
	<React.Fragment>
		<AnimatePresence>
			{ toggleMenu && (
			<Nav
				initial={{x: "-100%"}}
				exit={{
					x: "-100%"
				}}
				animate={{
					x: toggleMenu ? 0 : "-100%",
				}}
				transition={{
					duration: 1,
					ease: [0.6,.05,-0.01,.9]
				}}>
			<Container>
				<NavHeader>
					<Flex spaceBetween noHeight>
						<h2>Projects</h2>
						<CloseNav
							onClick={ () => setToggleMenu(!toggleMenu) }
						>
							<button>
								<span></span>
								<span></span>
							</button>
						</CloseNav>
					</Flex>
				</NavHeader>
				<NavList>
					<ul>

					{navRoutes.map(nav=>(
						<motion.li 
							key={ nav.id }
							onHoverStart={() =>setRevealVideo({
								show: true,
								video: nav.video,
								key: nav.id
							})}
							onHoverEnd={() =>setRevealVideo({
								show: false,
								video: nav.video,
								key: nav.id
							})}>
							<a 
								href={ nav.path }
								target="_blank"
								rel="noopener noreferrer">
								<motion.div 
									initial={{x: -48}}
									whileHover={{
										x: -12,
										transition:{
											duration: .4,
											ease: [0.6,.05,-0.01,.9]
										}
									}}
									className="link">
									<span className="arrow">
										 <svg
											xmlns="http://www.w3.org/2000/svg"
											viewBox="0 0 101 57"
											>
											<path
											  d="M33 34H0V24h81.429L66 7.884 73.548 0l19.877 20.763.027-.029L101 28.618 73.829 57l-7.548-7.884L80.753 34H33z"
											  fill="#FFF"
											  fillRule="evenodd"
											></path>
											</svg> 
									</span>
									{nav.title}
								</motion.div>
							</a>
						</motion.li>
					))}
					</ul>
				</NavList>
				<NavFooter></NavFooter>
				<NavVideos>
					<motion.div 
						animate={{
							width: revealVideo.show ? 0 : "100%",
							transition: {
								duration: 1,
								ease: [0.6,.05,-0.01,.9]
							}
						}}
					className="reveal"></motion.div>
					<div className="video">
						<AnimatePresence
							initial={false}
							exitBeforeEnter
						>
							<motion.video 
							key={ revealVideo.key }
							src={require(`../assets/videos/${revealVideo.video}`)}
							initial={{opacity: 0}}
							exit={{opacity: 0}}
							animate={{opacity: 1,
								transition: {
									duration: .2,
									ease: "easeInOut"
								}
							}}
							loop
							autoPlay
							muted 
							>
							</motion.video>
						</AnimatePresence>
					</div>
				</NavVideos>
			</Container>
		</Nav>
			)}
		</AnimatePresence>
	</React.Fragment>
	)
}
export default Navigation;