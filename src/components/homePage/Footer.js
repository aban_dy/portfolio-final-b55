import React from 'react';
import { motion } from 'framer-motion';
import { FooterContainer,ContactDetails, SocialLinks } from '../../styles/footerStyles';

//intersection-observer
import { useInView } from 'react-intersection-observer';

const footer = {
	initial:{ 
		opacity: 0,
		y: 100
	},
	animate:{
		opacity: 1,
		y: 0,
		transition:{
			duration: 1,
			ease: [ 0.6, 0.05, -0.01, 0.9 ]
		}
	}
}

const Footer = () => {
const [ref, inView] = useInView({ threshold: .5 });

return(
		<React.Fragment>
			<FooterContainer
				ref={ ref }
				variants={ footer }
				initial="initial"
				animate={ inView ? "animate" : "initial" }
			>
				
				<SocialLinks>

					<motion.div 
					initial={{ x: -48 }}
					whileHover={{
						x: -12,
						color: "#FF6347",
						transition:{
							duration: .4,
							ease: [0.6,.05,-0.01,.9]
						}
					}}
					className="social">
						<span className="arrow">
							 <svg
								xmlns="http://www.w3.org/2000/svg"
								viewBox="0 0 101 57"
								>
								<motion.path
								  d="M33 34H0V24h81.429L66 7.884 73.548 0l19.877 20.763.027-.029L101 28.618 73.829 57l-7.548-7.884L80.753 34H33z"
								  fill="#FFF"
								  fillRule="evenodd"
								></motion.path>
								</svg> 
						</span>
						<a 
							href="https://gitlab.com/aban_dy/portfolio-final-b55"
							target="_blank"
							rel="noopener noreferrer">portfolio source code</a>
					</motion.div>

					<motion.div 
					initial={{ x: -48 }}
					whileHover={{
						x: -12,
						color: "#FF6347",
						transition:{
							duration: .4,
							ease: [0.6,.05,-0.01,.9]
						}
					}}
					className="social">
						<span className="arrow">
							 <svg
								xmlns="http://www.w3.org/2000/svg"
								viewBox="0 0 101 57"
								>
								<path
								  d="M33 34H0V24h81.429L66 7.884 73.548 0l19.877 20.763.027-.029L101 28.618 73.829 57l-7.548-7.884L80.753 34H33z"
								  fill="#FFF"
								  fillRule="evenodd"
								></path>
								</svg> 
						</span>
						<a 
							href="https://gitlab.com/aban_dy" 
							target="_blank"
							rel="noopener noreferrer">gitlab</a>
					</motion.div>

					<motion.div 
					initial={{ x: -48 }}
					whileHover={{
						x: -12,
						color: "#FF6347",
						transition:{
							duration: .4,
							ease: [0.6,.05,-0.01,.9]
						}
					}}
					className="social">
						<span className="arrow">
							 <svg
								xmlns="http://www.w3.org/2000/svg"
								viewBox="0 0 101 57"
								>
								<path
								  d="M33 34H0V24h81.429L66 7.884 73.548 0l19.877 20.763.027-.029L101 28.618 73.829 57l-7.548-7.884L80.753 34H33z"
								  fill="#FFF"
								  fillRule="evenodd"
								></path>
								</svg> 
						</span>
						<a 
							href="https://www.linkedin.com/in/dy-aban" 
							target="_blank"
							rel="noopener noreferrer" >linkedin</a>
					</motion.div>

				</SocialLinks>

				<ContactDetails>
					<div className="contact">
						<h1>email</h1>
						<p>dinoaban@gmail.com</p>
					</div>
					<div className="contact">
						<h1>phone</h1>
						<p>+639182642472</p>
					</div>
				</ContactDetails>

			</FooterContainer>
		</React.Fragment>
	)
}

export default Footer;