import React from 'react';
import { 
	ProjectContainer,
	Grid,
	ProjectItem,
	Video,
	ProjectTitle,
	Actions 
} from '../../styles/projectStyles';

//global context
import { useGlobalStateContext, useGlobalDispatchContext } from '../../context/globalContext';

//intersection observer
import { useInView } from 'react-intersection-observer';
const parent = {
	initial:{
		opacity: 0
	},
	animate:{
		opacity: 1,
		transition:{
			duration: 1,
			staggerChildren: .2
		}
	}
}

const child = {
	initial:{
		opacity: 0,
		y:'100%',
		transition:{
			duration: .2
		}
	},
	animate:{
		opacity: 1,
		y: 0,
		transition:{
			duration: 1,
			ease: [ 0.6, 0.05, -0.01, 0.9 ],
		}
	}
}
const Project = () => {
	const { cursorStyles } = useGlobalStateContext();
	const dispatch = useGlobalDispatchContext();
	const [ref,inView] = useInView({ threshold: .2 });

	React.useEffect(()=>{
		if(inView){
			dispatch({type:'CURRENT_PAGE', page:"projects"})
		}
	},[inView,dispatch])

	const onCursor = cursorType => {
		cursorType = (cursorStyles.includes(cursorType) && cursorType) || false;
		dispatch({type: 'CURSOR_TYPE', cursorType: cursorType});
	}
return(
	<React.Fragment>
		<ProjectContainer 
			ref={ ref }
			initial="initial"
			variants={ parent }
			animate={ inView ? "animate" : "initial" }>
			<h1>Projects</h1>

			<Grid
				initial="initial"
				animate={ inView ? "animate" : "initial" }
				variants={ parent }>

				<ProjectItem variants={ child }>
					<Video>
						<video
							controls
							muted
							src={require('../../assets/videos/flames.mp4')}
						/>
					</Video>
					<ProjectTitle>
						<h1>Flames</h1>
						<p>Simple web app written in react.js</p>
					</ProjectTitle>
					<Actions>
						<button
							onMouseEnter={()=>onCursor('pointer')}
							onMouseLeave={onCursor}
						>
							<a 
								href="https://dy-flames-app.herokuapp.com/"
								target="_blank"
								rel="noopener noreferrer">view source</a>
						</button>
						<button
							onMouseEnter={()=>onCursor('pointer')}
							onMouseLeave={onCursor}
							className="view-page"
						>
							<a 
								href="https://dy-flames-app.herokuapp.com/"
								target="_blank"
								rel="noopener noreferrer">view page</a>
						</button>
					</Actions>
				</ProjectItem>

				<ProjectItem variants={ child }>
					<Video>
						<video
							controls
							muted
							src={require('../../assets/videos/washly.mp4')}
						/>
					</Video>
					<ProjectTitle>
						<h1>Washly</h1>
						<p>Static website for laundry services. Written in pure css3 and html5</p>
					</ProjectTitle>
					<Actions>
						<button
							onMouseEnter={()=>onCursor('pointer')}
							onMouseLeave={onCursor}
						>
							<a 
								href="https://gitlab.com/aban_dy/capstone-1"
								target="_blank"
								rel="noopener noreferrer">view source</a></button>
						<button
							onMouseEnter={()=>onCursor('pointer')}
							onMouseLeave={onCursor}
							className="view-page"
						>
							<a 
								href="https://aban_dy.gitlab.io/capstone-1"
								target="_blank"
								rel="noopener noreferrer">view page</a>
						</button>
					</Actions>
				</ProjectItem>

				<ProjectItem variants={ child }>
					<Video>
						<video
							controls
							muted
							src={require('../../assets/videos/polster.mp4')}
						/>
					</Video>
					<ProjectTitle>
						<h1>Polster</h1>
						<p>Dynamic website for employee management systyem. Made in laravel and sql</p>
					</ProjectTitle>
					<Actions>
						<button
							onMouseEnter={()=>onCursor('pointer')}
							onMouseLeave={onCursor}
						>
							<a 
								href="https://gitlab.com/aban_dy/capstone-2"
								target="_blank"
								rel="noopener noreferrer">view source</a>
						</button>
						<button
							onMouseEnter={()=>onCursor('pointer')}
							onMouseLeave={onCursor}
							className="view-page"
						>
							<a 
								href="https://glacial-atoll-69513.herokuapp.com/"
								target="_blank"
								rel="noopener noreferrer">view page</a>
						</button>
					</Actions>
				</ProjectItem>

				<ProjectItem variants={ child }>
					<Video>
						<video
							controls
							muted
							src={require('../../assets/videos/orange.mp4')}
						/>
					</Video>
					<ProjectTitle>
						<h1>Orange</h1>
						<p>Renting car web app. Made with React, Node and MongoDb</p>
					</ProjectTitle>
					<Actions>
						<button
							onMouseEnter={()=>onCursor('pointer')}
							onMouseLeave={onCursor}
						>
							<a 
								href="https://gitlab.com/aban_dy/capstone-3-frontend"
								target="_blank"
								rel="noopener noreferrer">view source</a>
						</button>
						<button
							onMouseEnter={()=>onCursor('pointer')}
							onMouseLeave={onCursor}
							className="view-page"
						>
							<a 
								href="https://capstone-3-frontent.netlify.app/"
								target="_blank"
								rel="noopener noreferrer">view page</a>
						</button>
					</Actions>
				</ProjectItem>

			</Grid>
		</ProjectContainer>
	</React.Fragment>
	)
}

export default Project;