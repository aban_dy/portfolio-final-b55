import React from 'react';

//images
import thinkImage from '../../assets/images/thinkOutsideTheBox.jpg';
import working from '../../assets/images/working.jpg'

//styled components
import { 
	AboutContainer, 
	Inner,
	Content,
	Images,
	TopRight,
	TopLeft,
	Text,
	BlackBox,
	BoxOverlay
} from '../../styles/aboutStyles.js';


//intersection-observer
import { useInView } from 'react-intersection-observer';

import { useGlobalDispatchContext } from '../../context/globalContext';


const About = () => {
const dispatch = useGlobalDispatchContext();
const [ref,inView] = useInView({ threshold: .2 });

React.useEffect(()=>{
if(inView){
	dispatch({type:"CURRENT_PAGE", page:"about"});
}
},[inView,dispatch])

const content = {
	initial:{
		opacity: 0,
		y: '100%',
		transition: {
			duration: 1,
			ease: [ 0.6, 0.05, -0.01, 0.9 ]
		}
	},
	animate:{
		opacity: 1,
		y: 0,
		transition: {
			duration: 1,
			ease: [ 0.6, 0.05, -0.01, 0.9 ]
		}
	}
}

const images = {
	initial:{
		opacity: 0,
		x: '100%'
	},
	animate:{
		opacity: 1,
		x: 0,
		transition: {
			duration: 2,
			ease: [ 0.6, 0.05, -0.01, 0.9 ],
			staggerChildren: .4
		}
	}
}

const imageChild = {
	initial:{
		opacity: 0,
		x: '100%'
	},
	animate:{
		x: 0,
		opacity: 1,
		transition: {
			duration: 1,
			ease: [ 0.6, 0.05, -0.01, 0.9 ],
		}
	}
}

return(
	<React.Fragment>
		<AboutContainer ref={ ref }>
			<Inner>
				<Content
					initial="initial"
					variants={ content }
					animate={ inView ? "animate" : "initial" }
				>
					<Text>
						<h1>About Me</h1>
						<p>Hi!, I'm Gerardino Aban an aspiring web developer and a Tech enthusiast. As techonology evolves, new challenges arise and I'm up for that. I love learning new skills. Love to play instruments while not coding.</p>
						<h3>The ability to create</h3>
						<p>"Creativity doesn't wait for that perfect moment. It fashions its own perfect moments out of ordinary ones." -- Bruce Garrabrandt</p>
					</Text>	
				</Content>
				<Images
					initial="initial"
					variants={ images }
					animate={ inView ? "animate" : "initial" }
				>
					<TopRight variants={ imageChild }>
						<img src={ thinkImage } alt="#" />
					</TopRight>
					<TopLeft variants={ imageChild }>
						<img src={ working } alt="#" />
					</TopLeft>
					<BlackBox/>
					<BoxOverlay/>
				</Images>
			</Inner>
		</AboutContainer>
	</React.Fragment>
	)
}
export default About;