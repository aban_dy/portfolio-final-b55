import React from 'react';
//styled components
import { ContactContainer, Form, Video } from '../../styles/contactStyles';

//intersection-observer
import { useInView } from 'react-intersection-observer';

//global context
import { useGlobalStateContext, useGlobalDispatchContext } from '../../context/globalContext';

//email
import emailjs from 'emailjs-com';

//validator
import  EmailValidator  from 'email-validator';

//toast
import Toast from '../Toast';


const parent = {
	initial: { opacity: 0 },
	animate:{
		opacity: 1,
		transition:{
			duration: 1,
			staggerChildren: .2
		}
	}
}

const child = {
	initial: { 
		opacity: 0,
		y: "100%"
	},
	animate:{
		opacity: 1,
		y: 0,
		transition:{
			duration: 1,
			ease: [ 0.6, 0.05, -0.01, 0.9 ],
		}
	}
}

const Contact = () => {
	const { cursorStyles } = useGlobalStateContext();
	const dispatch = useGlobalDispatchContext();
	const [ref,inView] = useInView({ threshold: .5 });

	//states
	const [name,setName] = React.useState("");
	const [email,setEmail] = React.useState("");
	const [message,setMessage] = React.useState("");
	const [err,setErr] = React.useState(false);
	const [errorMessage, setErrorMessage] = React.useState("");
	const [toastMessage,setToastMessage] = React.useState("");
	const [isOpen, setIsOpen] = React.useState(false);

	React.useEffect(()=>{
		if(inView){
			dispatch({type:'CURRENT_PAGE', page: 'contact'});
		}
	},[inView,dispatch])

	const onCursor = cursorType => {
		cursorType = (cursorStyles.includes(cursorType) && cursorType) || false;
		dispatch({type: 'CURSOR_TYPE', cursorType: cursorType});
	}

	const handleSumbit = async () => {

		if(name.length === 0 || email.length === 0 || message.length === 0){
			setErr(true);
			setToastMessage("Oops! an error occured");
			setIsOpen(true);
		}else{
			// this is the body of email to be sent
			const template_params = {
				"reply_to": email,
				"from_name": name,
				"to_name": "Portfolio_Message",
				"message_html": message
			}
			const user_id = "user_Yn7scwfbgY5QhSSop5i9V";
			const service_id = "default_service";
			const template_id = "template_023j2wXt";
			const res = await emailjs.send(service_id, template_id, template_params, user_id);
			if(res.status === 200){
				setToastMessage("Thank you for your message!");
				setIsOpen(true);
				setName("");
				setEmail("");
				setMessage("");
			}else{
				setToastMessage("Oops! an error occured");
				setIsOpen(true);
			}
		}

	}

	const handleOnBlur = () => {
		if(EmailValidator.validate(email) === false){
			setErrorMessage("Invalid Email");
		}else{
			setErrorMessage("");
		}
	}


 return(
 	<React.Fragment>
 		<ContactContainer 
 			ref={ ref } 
 			initial="initial"
 			variants={ parent }
 			animate={ inView ? "animate" : "" }>
 			<Video variants={ child }>
 				<h1>let's talk about it now</h1>
 				<video 
 					src={require('../../assets/videos/deal.mp4')} 
 					controls
 					muted
 					loop
 					autoPlay
 					/>
 			</Video>
 			<Form variants={ child }>
 				<h1>send a message</h1>
 				<div className="form-group">
 					<div className="form">
 						<label>Name</label>
 						<input 
 							type="text" 
 							placeholder="Your name"
 							onChange={(e) => setName(e.target.value)}
 							value={ name }/>
 							<strong>{err ? `${ name.length === 0 ? "Thil field is required" : "" }` : "" }</strong>
	 				</div>
	 				<div className="form">
	 					<label>Email</label>
	 					<input 
	 						type="email" 
	 						placeholder="Your email"
	 						onChange={(e) => setEmail(e.target.value)}
	 						value={ email }
	 						onBlur={ handleOnBlur }/>
	 						<strong>{ errorMessage } </strong>
	 				</div>
 				</div>
 				<div className="form">
 					<label>Message</label>
 					<textarea 
 						placeholder="You message"
 						onChange={(e) => setMessage(e.target.value)}
 						value={ message }/>
 						<strong>{err ? `${ message.length === 0 ? "Thil field is required" : "" }` : "" }</strong>
 				</div>
 				<button 
 				onMouseEnter={()=>onCursor('pointer')}
 				onMouseLeave={onCursor}
 				onClick={ handleSumbit }
 				className="btn">
 					submit
 				</button>
 			</Form>
 		</ContactContainer>
 		
 		<Toast toastMessage={ toastMessage } isOpen={ isOpen } setIsOpen={ setIsOpen } />

 	</React.Fragment>
 	)
}

export default Contact;