import React from 'react';

//styled component
import { SkillsContainer,SkillsTitle, Grid, Tile } from '../../styles/skillStyles';
import { Award } from '../../assets/svg/social-icons';

//intersection-observer
import { useInView } from 'react-intersection-observer';

//global context
import { useGlobalDispatchContext } from '../../context/globalContext';

const skills = [
	{ id: 0 , name: "html", y: 50 },
	{ id: 1 , name: "css", y: 40 },
	{ id: 2 , name: "js", y: 30 },
	{ id: 3 , name: "mongodb", y: 20 },
	{ id: 4 , name: "react", y: 10 },
	{ id: 5 , name: "node", y: 5 },
]

const parent = {
	initial:{ opacity: 0 },
	animate:{
		opacity: 1,
		transition: {
			duration: 1,
			staggerChildren: .2
		}
	}
}

const child = {
	initial:{ 
		opacity: 0,
		y: 100
	},
	animate:{
		opacity: 1,
		y: 0,
		transition: {
			duration: 1,
			ease: [ 0.6, 0.05, -0.01, 0.9 ],
		}
	}
}

const Skills = () => {
	const dispatch = useGlobalDispatchContext();
	const [ref, inView] = useInView({ threshold: .2 });

	React.useEffect(()=>{
		if(inView){
			dispatch({type:'CURRENT_PAGE', page:"skills"});
		}
	},[inView,dispatch])
	return(
		<React.Fragment>
			<SkillsContainer 
				ref={ ref }
				initial="initial"
				variants={ parent }
				animate={ inView ? "animate" : "initial" }>
				<SkillsTitle>
						<h1>{"Skills"}</h1>
				</SkillsTitle>
				<Grid>
					{skills.map(skill=>(
						<Tile 
						key={ skill.id } 
						variants={ child }>
								<Award />
								<span>{skill.name}</span>
						</Tile>
					))}
				</Grid>
			</SkillsContainer>
		</React.Fragment>
	)
}
export default Skills