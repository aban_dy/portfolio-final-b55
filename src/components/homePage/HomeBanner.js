import React from 'react';
import { 
		Banner,
		Video, 
		BannerTitle,
		HeadLine,
		} from '../../styles/homeStyles';

const parent = {
	initial : { y : 800},
	animate: {
		y: 0,
		transition: {
			staggerChildren: .2
		}
	}

}

const child = {
	initial : { y : 800 },
	animate: {
		y: 0,
		transition: {
			duration: 1,
			ease: [ 0.6, 0.05, -0.01, 0.9 ]
		}
	}
}

const HomeBanner = () => {
	return(
		<Banner>
			<Video>
				<video 
					loop
					autoPlay
					muted
					src={require('../../assets/videos/typing.mp4')}
				/>
			</Video>
				<BannerTitle
					variants={ parent }
					initial="initial"
					animate="animate">
						<HeadLine variants={ child } >{"DEV"}</HeadLine>
						<HeadLine variants={ child } >{"ELOPER"}</HeadLine>
				</BannerTitle>
		</Banner>
	)
}

export default HomeBanner;

// <video 
// height='100%'
// width='100%'
// loop
// autoPlay
// muted
// src={require('../../assets/videos/typing.mp4')}
// />