import React,{ useState } from 'react';
import PropTypes from 'prop-types';

//Components
import Header from './Header';
import CustomCursor from './CustomCursor';
import Navigation from './Navigation';

//Styled components
import { createGlobalStyle, ThemeProvider } from 'styled-components';
import { normalize } from 'styled-normalize';

//Context
import { useGlobalStateContext,useGlobalDispatchContext } from '../context/globalContext';

const GlobalStyle = createGlobalStyle`
	${ normalize }
	*{
		text-decoration: none;
		cursor: none;
		@media(max-width: 768px){
			cursor: crosshair;
		}
	}

	html{
		box-sizing: border-box;
		scroll-behavior: smooth;
		-webkit-font-smoothing: antialiased;
		font-size: 16px;
	}

	body{
		font-family: 'Poppins', sans-serif;
		background: ${ ({ theme }) => theme.background };
		color: ${ ({theme}) => theme.text };
		overscroll-behavior: none;
		overflow-x: hidden;
		position: relative;
	}
`

const darkTheme = {
	background: '#000',
	text: '#fff',
	red: '#FF6347'
}

const lightTheme = {
	background: '#fff',
	text: '#000',
	red: '#FF6347'
}

const Layout = ({ children }) => {

	const { currentTheme, cursorStyles } = useGlobalStateContext();
	const dispatch = useGlobalDispatchContext();

	const onCursor = cursorType => {
		cursorType = (cursorStyles.includes(cursorType) && cursorType) || false;
		dispatch({type: 'CURSOR_TYPE', cursorType: cursorType});
	}

	const [toggleMenu, setToggleMenu] = useState(false);  

	return (
		<React.Fragment>
			<ThemeProvider 
				theme={currentTheme === 'dark' ? darkTheme : lightTheme}>
				<GlobalStyle />
				<CustomCursor toggleMenu={toggleMenu}/>
				<Header 
					onCursor={ onCursor }
					toggleMenu={ toggleMenu }
					setToggleMenu={ setToggleMenu }/>
				<Navigation 
					toggleMenu={ toggleMenu }
					setToggleMenu={ setToggleMenu }/>
				<main>{ children }</main>
			</ThemeProvider>
		</React.Fragment>
	)
}
Layout.propTypes = {
	children: PropTypes.node.isRequired,
}
export default Layout;