import React from 'react';

//styled components
import { FixedContainer } from '../styles/fixedStyles';

//intersection-observer
import { useInView } from 'react-intersection-observer';

//framer-motion
import { motion, AnimatePresence } from 'framer-motion';

//global contex
import { useGlobalStateContext } from '../context/globalContext';

const parent = {
	initial: {
		opacity: 0,
		y: -100
	},
	animate: {
		opacity: 1,
		y: 0,
		transition: {
			duration: .5,
			ease: [ 0.6, 0.05, -0.01, 0.9 ],
			staggerChildren: .2
		}
	}
}

const child = {
	initial: {
		opacity: 0
	},
	animate: {
		opacity: 1,
		rotateY: 360,
		transition: {
			duration: .5,
			ease: [ 0.6, 0.05, -0.01, 0.9 ],
		}
	},
	exit:{
		opacity: 0,
		rotateY: 360,
		transition: {
			duration: .5,
			ease: [ 0.6, 0.05, -0.01, 0.9 ],
		}
	}
}
const Fixed = () => {

const [ref,inView] = useInView({ threshold: 0 });
const { currentPage } = useGlobalStateContext();


return(
		<FixedContainer
			ref={ ref }
		>
		<AnimatePresence exitBeforeEnter>
			<motion.ul
				initial="initial"
				variants={ parent }
				animate={ inView ?  "animate" : "initial" }
			>

				{currentPage?.split("").map((letter,index) => (
					<motion.li
						key={ index }
						variants={ child }
						exit="exit"
					>{ letter.toUpperCase() }</motion.li>
				))}

				<motion.span
					variants={ child }
					exit="exit"
				></motion.span>
			</motion.ul>
		</AnimatePresence>
		</FixedContainer>
	)
}

export default Fixed;